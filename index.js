/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

// PART 1



function register(newUser){

                if ( registeredUsers.includes(newUser)){
                    alert("Registration failed. Username already exists!");
                }
                else {
                    registeredUsers.push(newUser);
                    alert("Thank you for registering!");
                    console.log(registeredUsers);
                }

}


    

// PART 2

function addFriend(newFriend){

                if ( registeredUsers.includes(newFriend)){
                    friendsList.push(newFriend);
                    alert("You have added " +newFriend+ " as a friend!");
                    console.log(friendsList);
                }

                else {
                        alert("User not found.");
                }

}






// PART 3

friendsList.forEach(displayFriends);


    function displayFriends(){
                if (friendsList.length === 0) {
                    alert("You currently have 0 friends. Add one first.");
                }
                
            friendsList.forEach(function(friends){
      
                if (true) {} {
                    console.log(friends);
                }
         
});

}


// PART 4


function displayNumberFriends(){
    let numberOfFriends = friendsList.length;
            if (friendsList.length === 0) {
                alert("You currently have 0 friends. Add one first.");
            }
                else {
                    alert("You currently have " +numberOfFriends+ " friends");
                    console.log(friendsList);
                }
}

// PART 5

function deleteFriend(){

    deletedFriend = friendsList.pop();
            if (friendsList.length === 0) {
                alert("You currently have 0 friends. Add one first.");
            }
                else {
                    alert("You have deleted " +deletedFriend+ " to your friends list");
                    console.log(friendsList);
                }
}


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function deletedFriendByIndex(index){
     let changeValue = friendsList.splice(index, 1);
    }






